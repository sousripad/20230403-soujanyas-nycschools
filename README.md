# 20230403-SoujanyaS-NYCSchools

## Name
20230403-SoujanyaSripad-NYCSchools

## Description
This project lists high schools in NYC and the details of each high schools highlighting the SAT results of the school and additional details about the high school. The data is fetched from the APIs from NYC Directory. This app using MVVM architecture.


## Visuals
App screenshots can be found in the app/App Screenshots folder


## Installation
Built and Tested in Xcode Version 12.4 (12D4e) , Swift 5, Mac OS Catalina version 10.15.7

## Usage
The app helps to find a particular high school from the list of high schools in NYC.
The search feature is specifically useful to look up for familiar schools and find more details.

## Support
Any queries I can be reached at
sousripad@gmail.com

## Roadmap
If you have ideas for releases in the future, it is a good idea to list them in the README.
Given more time the following features could make the app useful and convenient
- Filtering schools based on user's location
- Marking schools as favourite for easy reference
- Adding more details of the school given in the API 
- SAT comparison between schools

## Authors and acknowledgment
Soujanya Sripad


