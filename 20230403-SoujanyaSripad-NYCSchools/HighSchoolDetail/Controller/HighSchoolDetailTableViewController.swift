//
//  HighSchoolDetailTableViewController.swift
//  20230403-SoujanyaSripad-NYCSchools
//
//  Created by Soujanya on 11/03/23.
//

import UIKit


//This class handles the display of list of schools with a search option.

//Given more time, I would prefer to setup the location directions to the school, add more information about the school like languages offered etc

//Following a MVVM model where in the processing and fetching of UI element values is taken care in the view model

class HighSchoolDetailTableViewController: UITableViewController {
    
    fileprivate var viewModel: HighSchoolDetailViewModel!
    weak var delegate: HighSchoolDetailViewModelDelegate?
    var selectedSchool : HighSchool?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Uncomment the following line to preserve selection between presentations
        viewModel = HighSchoolDetailViewModel()
        self.clearsSelectionOnViewWillAppear = false
        tableView.sizeToFit()
        
        
        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
    }
    
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return viewModel.totalSections()
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return viewModel.totalRows()
    }
    
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        // Configure the cell...
        switch indexPath.row {
        case 0:
            let cell = tableView.dequeueReusableCell(withIdentifier: "DetailSATScoreTableViewCell", for: indexPath) as! DetailSATScoreTableViewCell
            if let school = selectedSchool {
                cell.configure(HighSchoolTableCellViewModel(school))
            }
            return cell
            
        case 1:
            let cell = tableView.dequeueReusableCell(withIdentifier: "DetailOverviewTableViewCell", for: indexPath) as! DetailOverviewTableViewCell
            if let school = selectedSchool {
                cell.configure(HighSchoolTableCellViewModel(school))
            }
            return cell
            
        case 2:
            let cell = tableView.dequeueReusableCell(withIdentifier: "DetailContactTableViewCell", for: indexPath) as! DetailContactTableViewCell
            if let school = selectedSchool {
                cell.configure(HighSchoolTableCellViewModel(school))
            }
            return cell
            
        case 3:
            let cell = tableView.dequeueReusableCell(withIdentifier: "DetailMapTableViewCell", for: indexPath) as! DetailMapTableViewCell
            if let school = selectedSchool {
                cell.configure(HighSchoolTableCellViewModel(school))
            }
            return cell
            
        default:
            let cell = tableView.dequeueReusableCell(withIdentifier: "DetailOverviewTableViewCell", for: indexPath) as! DetailOverviewTableViewCell
            if let school = selectedSchool {
                cell.configure(HighSchoolTableCellViewModel(school))
            }
            return cell
        }
    }
    
    override func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
}
