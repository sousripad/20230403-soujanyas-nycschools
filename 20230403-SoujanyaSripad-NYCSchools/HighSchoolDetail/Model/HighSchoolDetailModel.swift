//
//  HighSchoolDetailModel.swift
//  20230403-SoujanyaSripad-NYCSchools
//
//  Created by Soujanya on 08/03/23.
//

import Foundation

//Mapping the sat result response in this view model

struct SATResults : Codable {
    let dbn : String
    let school_name : String
    let sat_critical_reading_avg_score: String
    let sat_math_avg_score: String
    let sat_writing_avg_score: String
}
