//
//  HighSchoolDetailViewModel.swift
//  20230403-SoujanyaSripad-NYCSchools
//
//  Created by Soujanya on 08/03/23.
//

import Foundation


//This class takes care of providing data in the desired manner to the view, making network calls and avoids any unnecessary functions in controller making it loaded.

//It also maps the SAT scores from the second API call onto the struct of the first

protocol HighSchoolDetailViewModelDelegate : AnyObject {
    func error(with message: String)
    func reloadData()
    func mapScoresToSchools(scores: [SATResults])
}


class HighSchoolDetailViewModel {
    weak var delegate: HighSchoolDetailViewModelDelegate?
    private var schools: [HighSchool] = []
    var satResults: [SATResults] = []
    var selectedSchool : HighSchool?
    
    func getSATResults() {
       NetworkManager.getSATResults(completion: {[weak self] (errorMessage, satResults) in
            
            if errorMessage != nil {
                self?.delegate?.error(with: errorMessage!)
            }
            else {
                if let satResults = satResults
                {
                    self?.delegate?.mapScoresToSchools(scores: satResults)
                }               
            }
        })
        
    }
    
    func schoolName() -> String {
        return selectedSchool?.school_name ?? ""
    }

}

//MARK: - Table methods
extension HighSchoolDetailViewModel {
    func totalSections() -> Int {
        return 1
    }
    
    func totalRows() -> Int {
        return 4
    }
    
    func cellViewModel(at index: Int) -> HighSchoolTableCellViewModel {
        let school = schools[index]
        return HighSchoolTableCellViewModel(school)
    }

}
