//
//  DetailMapTableViewCell.swift
//  20230403-SoujanyaSripad-NYCSchools
//
//  Created by Soujanya on 08/03/23.
//

import UIKit
import MapKit


//The cell which configures the UI to show map with the pin dropped on the school and sets value upon delegate calls in the details of high school

class DetailMapTableViewCell: UITableViewCell {
    @IBOutlet weak var addressMapView: MKMapView!
    private var viewModel: HighSchoolTableCellViewModel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
        // Configure the view for the selected state
        func configure(_ viewModel: HighSchoolTableCellViewModel) {
            self.viewModel = viewModel
            if let coordinates = viewModel.coordinates {
                addHighSchoolAnnotaionWithCoordinates(coordinates)
            }
        }
    
    func addHighSchoolAnnotaionWithCoordinates(_ hsCoordinates: CLLocationCoordinate2D){
        let highSchoolAnnotation = MKPointAnnotation()
        highSchoolAnnotation.coordinate = hsCoordinates
        self.addressMapView.addAnnotation(highSchoolAnnotation)
        let span = MKCoordinateSpan(latitudeDelta: 0.001, longitudeDelta: 0.001)
        let region = MKCoordinateRegion(center: highSchoolAnnotation.coordinate, span: span)
        let adjustRegion = self.addressMapView.regionThatFits(region)
        self.addressMapView.setRegion(adjustRegion, animated:true)
    }

}
