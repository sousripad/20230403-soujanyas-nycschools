//
//  DetailOverviewTableViewCell.swift
//  20230403-SoujanyaSripad-NYCSchools
//
//  Created by Soujanya on 08/03/23.
//

import UIKit

//The cell which configures the UI to show the overview of the highschool and sets value upon delegate calls in the details of high school

class DetailOverviewTableViewCell: UITableViewCell {
    @IBOutlet weak var detailOverviewLabel: UILabel!
    @IBOutlet weak var detailOverviewText: UILabel!
    private var viewModel: HighSchoolTableCellViewModel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    // Configure the view  by passing values from respective view model
    func configure(_ viewModel: HighSchoolTableCellViewModel) {
        self.viewModel = viewModel
        self.detailOverviewLabel.text = "Overview"
        self.detailOverviewText.text = viewModel.overview
    }
    
}
