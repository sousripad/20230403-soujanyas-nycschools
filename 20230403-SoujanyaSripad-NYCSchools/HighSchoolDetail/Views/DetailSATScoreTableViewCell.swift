//
//  DetailSATScoreTableViewCell.swift
//  20230403-SoujanyaSripad-NYCSchools
//
//  Created by Soujanya on 08/03/23.
//

import UIKit

//The cell which configures the UI to show SAT scores and sets value upon delegate calls in the details of high school

class DetailSATScoreTableViewCell: UITableViewCell {
    @IBOutlet weak var schoolNameLabel: UILabel!
    @IBOutlet weak var satReadingLabel: UILabel!
    @IBOutlet weak var satMathLabel: UILabel!
    @IBOutlet weak var satWritingLabel: UILabel!
    
    @IBOutlet weak var satReadingAvgScore: UILabel!
    @IBOutlet weak var satMathAvgScore: UILabel!
    @IBOutlet weak var satWritingAvgScore: UILabel!
    
    @IBOutlet weak var labelStackView: UIStackView!
    @IBOutlet weak var scoreStackView: UIStackView!
    
    private var viewModel: HighSchoolTableCellViewModel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
    }
    
    // Configure the view for the selected state
    func configure(_ viewModel: HighSchoolTableCellViewModel) {
        self.viewModel = viewModel
        if viewModel.hasSATScore! {
            labelStackView.isHidden = false
            scoreStackView.isHidden = false
            self.schoolNameLabel.text = viewModel.schoolName
            self.satReadingAvgScore.text = viewModel.satAvgReadingScore
            self.satMathAvgScore.text = viewModel.satAvgMathScore
            self.satWritingAvgScore.text = viewModel.satAvgWritingScore
        }
        else {
            labelStackView.isHidden = true
            scoreStackView.isHidden = true
            self.schoolNameLabel.text = viewModel.schoolName + "\n" + HighSchoolListStrings.satScoreUnknown
        }
        self.schoolNameLabel.setContentCompressionResistancePriority(.required, for: .vertical)
        self.satReadingAvgScore.setRoundView()
        self.satMathAvgScore.setRoundView()
        self.satWritingAvgScore.setRoundView()
    }
    
    
    
}
