//
//  DetailContactTableViewCell.swift
//  20230403-SoujanyaSripad-NYCSchools
//
//  Created by Soujanya on 08/03/23.
//

import UIKit

//The cell which configures the UI to show show more contact details and sets value upon delegate calls in the details of high school


class DetailContactTableViewCell: UITableViewCell {
    private var viewModel: HighSchoolTableCellViewModel!
    @IBOutlet weak var schoolAddress: UILabel!
    @IBOutlet weak var schoolWebsite: UILabel!
    @IBOutlet weak var schoolPhoneNumber: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    // Configure the view  by passing values from respective view model
    func configure(_ viewModel: HighSchoolTableCellViewModel) {
        self.viewModel = viewModel
        self.schoolAddress.text = viewModel.address
        self.schoolWebsite.text = viewModel.website
        self.schoolPhoneNumber.text = viewModel.phoneNumber
        
        //Setting tap getsure on UILabel to open the school website
        let clickOnwebsite = UITapGestureRecognizer(target: self, action: #selector(DetailContactTableViewCell.clickOnWebsite))
        self.schoolWebsite.isUserInteractionEnabled = true
        self.schoolWebsite.addGestureRecognizer(clickOnwebsite)
        
        //Setting tap getsure on UILabel to place a call on click on phone number
        let clickOnPhoneNumber = UITapGestureRecognizer(target: self, action: #selector(DetailContactTableViewCell.clickOnPhoneNumber))
        self.schoolPhoneNumber.isUserInteractionEnabled = true
        self.schoolPhoneNumber.addGestureRecognizer(clickOnPhoneNumber)
    }
    
    @objc func clickOnWebsite(sender: UITapGestureRecognizer) {
        if(sender.view == self.schoolWebsite)
        {
            if let website = self.schoolWebsite.text, let url = URL(string: "https://" + website) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            }
        }
    }
    
    @objc func clickOnPhoneNumber(sender: UITapGestureRecognizer) {
        print("tap working")
        if(sender.view == self.schoolPhoneNumber)
        {
            if let phone = self.schoolPhoneNumber.text, let url = URL(string: "tel://+1" + phone.filter{$0.isNumber}) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            }
        }
    }
    
}
