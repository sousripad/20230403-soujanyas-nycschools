//
//  NetworkModel.swift
//  20230303-SoujanyaSripad-NYCSchools
//
//  Created by Soujanya on 04/03/23.
//

import Foundation

// This struct is a model to map/define a response we typically receive from the API call including the response, errors, exceptions and messages from the network.

//Given more time, I would prefer to mapp all network calls response for easy access and uniformity. 

struct NetworkResponse {
    var code: Int
    var error: Bool
    var message: String?
    var content: [String: Any]?
    var exception: String?
    
    static func encode(json: [String: Any]) -> NetworkResponse? {
        guard let code = json["code"] as? Int, let error = json["error"] as? Bool else { return nil }
        let response = NetworkResponse(code: code, error: error, message: json["message"] as? String, content: json["content"] as? [String: Any], exception: json["exception"] as? String)
        return response
    }
    
    static func response(from data: Data) -> NetworkResponse? {
        guard let object = try? JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String: Any] else { return nil }
        return NetworkResponse.encode(json: object)
    }
}
