//
//  NetworkManager.swift
//  20230303-SoujanyaSripad-NYCSchools
//
//  Created by Soujanya on 04/03/23.
//

// This is a network manager wherein we make API calls and handle them accordingly. In our case we recieve the list of high schools and SAT results of schools so we make two API calls respectively.

import Foundation

class NetworkManager {
    
    //This function gets the list of high schools with details of the school from the API provided.
    //It also has a JSON decoder which decodes the recieved json into he Swift objects created in the Model class which conforms to Codable protocol.
    //If there was more time, I would love to include a Network Wrapper (included a network response mapping file(NetworkModel) but could not implement) which would take care of API requests in detail (Like building the URL, passing the httpMethod type)
    
    //If the decoding fails, there is a catch block which shows the detailed description of what went wrong.
    static func getHighSchoolList(completion: @escaping (_ errorMessage: String?,_ highSchools: [HighSchool]?) -> Void) {
        if let url = URL(string: AppUrl.gethighSchoolList) {
            var request = URLRequest(url: url)
            request.httpMethod = "GET"
            URLSession.shared.dataTask(with: request, completionHandler: { (highSchoolsData, response, error) in
                
                if let highSchoolsData = highSchoolsData {
                    do {
                        let highSchools = try JSONDecoder().decode([HighSchool].self, from: highSchoolsData)
                        return completion(error?.localizedDescription, highSchools)
                    } catch {
                        //fatalError(error.localizedDescription)
                        return completion(error.localizedDescription, nil)
                    }
                    } else if let error = error {
                        return completion(error.localizedDescription, nil)
                    }
            }).resume()
        }
    }
    
    
    static func getSATResults(completion: @escaping (_ errorMessage: String?,_ satResults: [SATResults]?) -> Void) {
        if let url = URL(string: AppUrl.getSATResults) {
            var request = URLRequest(url: url)
            request.httpMethod = "GET"
            URLSession.shared.dataTask(with: request, completionHandler: { (satResults, response, error) in
                
                if let satResults = satResults {
                    do {
                        let satResults = try JSONDecoder().decode([SATResults].self, from: satResults)
                        return completion(error?.localizedDescription, satResults)
                    } catch {
                        //fatalError(error.localizedDescription)
                        return completion(error.localizedDescription, nil)
                    }
                    } else if let error = error {
                        return completion(error.localizedDescription, nil)
                    }
            }).resume()
        }
    }
}
