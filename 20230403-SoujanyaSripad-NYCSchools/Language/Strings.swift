//
//  Strings.swift
//  20230303-SoujanyaSripad-NYCSchools
//
//  Created by Soujanya on 04/03/23.
//

import Foundation

//Keeping commonly used strings, used to be localised 

struct HighSchoolListStrings {
    static let ok = NSLocalizedString("ok", comment: "ok")
    static let cancel = NSLocalizedString("cancel", comment: "cancel")
    static let satScoreUnknown = NSLocalizedString("No SAT results found", comment: "No SAT results found")
    
    // Server Generic Error
    static let serverGenericError =  NSLocalizedString("serverGenericError", comment: "Server Generic Error")
    static let unableToProcessError = NSLocalizedString("unableToProcessError", comment: "Server Generic Error")
}
