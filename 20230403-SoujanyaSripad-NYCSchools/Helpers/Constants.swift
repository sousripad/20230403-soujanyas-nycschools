//
//  Constants.swift
//  20230303-SoujanyaSripad-NYCSchools
//
//  Created by Soujanya on 04/03/23.
//

import Foundation

//This swift file is dedicated to the constants of the app. Some constants like appURL(bas eurl usually) could be easily switched between production, QA respectively. Some app constants too for example Employee/User ID in case we have login based app etc.

struct AppUrl {
    
    static let baseUrl = "https://data.cityofnewyork.us/resource/"
    // API for list of NYC High Schools " "https://data.cityofnewyork.us/Education/DOE-High-School-Directory-2017/s3k6-pzi2""
    static let gethighSchoolList = "https://data.cityofnewyork.us/resource/s3k6-pzi2.json"
    
    // API for SAT results   "https://data.cityofnewyork.us/Education/SAT-Results/f9bf-2cp4"
    static let getSATResults = "https://data.cityofnewyork.us/resource/f9bf-2cp4.json"
    
    
    //API to filter locations based on user location (lat,long to be passed)
    static let getLocationBasedSchools = "https://data.cityofnewyork.us/resource/s3k6-pzi2.json?$where=within_circle(report_location,%2040.696457,%20-73.911751,%20500)"
}
