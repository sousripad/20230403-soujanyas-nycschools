//
//  AlertService.swift
//  20230403-SoujanyaSripad-NYCSchools
//
//  Created by Soujanya on 06/03/23.
//

import UIKit

//This class helps us present error messages/alerts in a customised way.

typealias AlertActionSelectionCallback = ((Int?) -> Void)
typealias AlertTextCallback = (([String]) -> Void)

class AlertService {
    static func addAlert(in vc: UIViewController?, with title: String!) {
        addAlert(in: vc, with: title, andActions: [HighSchoolListStrings.ok], completion: nil)
    }
    
    static func addAlert(in vc: UIViewController?, with title: String!, and message: String?) {
        addAlertWithCancel(in: vc, with: title, message: message, cancel: false, andActions: [HighSchoolListStrings.ok], completion: nil)
    }
    
    static func addAlert(in vc: UIViewController?, with title: String!, andActions actions:[String], completion: AlertActionSelectionCallback?) {
        addAlertWithCancel(in: vc, with: title, message: nil, cancel: false, andActions: actions, completion: completion)
    }
    
    static func addAlert(in vc: UIViewController?, with title: String!, message: String?, andActions actions:[String], completion: AlertActionSelectionCallback?) {
        addAlertWithCancel(in: vc, with: title, message: message, cancel: false, andActions: actions, completion: completion)
    }
    
    static func addAlertWithCancel(in vc: UIViewController?, with title: String!, andActions actions:[String], completion: AlertActionSelectionCallback?) {
        addAlertWithCancel(in: vc, with: title, message: nil, cancel: true, andActions: actions, completion: completion)
    }
    
    static func addAlertWithCancel(in vc: UIViewController?, with title: String!, message: String?, cancel: Bool, andActions actions:[String], completion: AlertActionSelectionCallback?) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        for action in actions {
            let alertAction = UIAlertAction(title: action, style: .default, handler: { (_) in
                completion?(actions.firstIndex(of: action))
            })
            alert.addAction(alertAction)
        }
        
        if cancel {
            let cancelAction = UIAlertAction(title: HighSchoolListStrings.cancel, style: .cancel, handler: nil)
            alert.addAction(cancelAction)
        }
        
        guard let controller = vc ?? UIApplication.topViewController() else { return }
        controller.present(alert, animated: true, completion: nil)
    }
    
    static func addAlertWithField(in vc: UIViewController?, with title: String, message: String?, fieldPlaceholders: [String], completion: AlertTextCallback?) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        for placeholder in fieldPlaceholders {
            alert.addTextField { textField in
                textField.placeholder = placeholder
                textField.keyboardType = .decimalPad
            }
        }
        
        let saveAction = UIAlertAction(title: HighSchoolListStrings.ok, style: .default) { action in
            if let allTextFields = alert.textFields {
                let texts = allTextFields.map { $0.text ?? "" }
                completion?(texts)
            }
        }
        alert.addAction(saveAction)
        
        let cancelAction = UIAlertAction(title: HighSchoolListStrings.cancel, style: .cancel, handler: nil)
        alert.addAction(cancelAction)

        guard let controller = vc ?? UIApplication.topViewController() else { return }
        controller.present(alert, animated: true, completion: nil)
    }
}

extension UIApplication {
    class func topViewController(controller: UIViewController? = nil) -> UIViewController? {
        let keyWindow = UIApplication.shared.windows.filter {$0.isKeyWindow}.first
        let rootController = controller ?? keyWindow?.rootViewController
        if let navigationController = rootController as? UINavigationController {
            return topViewController(controller: navigationController.visibleViewController)
        }
        if let presented = rootController?.presentedViewController {
            return topViewController(controller: presented)
        }
        return controller
    }
}

