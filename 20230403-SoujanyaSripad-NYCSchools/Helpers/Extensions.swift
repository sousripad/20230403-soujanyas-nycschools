//
//  Extensions.swift
//  20230403-SoujanyaSripad-NYCSchools
//
//  Created by Soujanya on 06/03/23.
//

import UIKit

//This file includes all extensions to the UIKit elements  which help the UI look more customized and nice!

extension UIView {
    func dropInnerShadow(cornerRadius: CGFloat? = nil) {
      /*  let innerShadow = CALayer()
        innerShadow.frame = bounds
        
        // Shadow path (1pt ring around bounds)
        let radius = cornerRadius ?? self.frame.size.height/2
        let path = UIBezierPath(roundedRect: innerShadow.bounds.insetBy(dx: -1, dy:-1), cornerRadius:radius)
        let cutout = UIBezierPath(roundedRect: innerShadow.bounds, cornerRadius:radius).reversing()
        
        
        path.append(cutout)
        innerShadow.shadowPath = path.cgPath
        innerShadow.masksToBounds = true
        // Shadow properties
        innerShadow.shadowColor = UIColor.purple.cgColor
        innerShadow.shadowOffset = CGSize(width: 0, height: 3)
        innerShadow.shadowOpacity = 0.3
        innerShadow.shadowRadius = 3
        innerShadow.cornerRadius = cornerRadius ?? self.frame.size.height/2
        layer.addSublayer(innerShadow)*/
        
        self.layer.cornerRadius = 20.0
        self.layer.shadowColor = UIColor.random.cgColor
        self.layer.shadowOffset = CGSize(width: 0, height: 2)
        self.layer.shadowOpacity = 0.8
        self.layer.shadowRadius = 3
        self.layer.masksToBounds = false
    }
    
    func setGradientBackground(with topColor: UIColor = .white, and bottomColor: UIColor = .black) {
        let gradientLayer = CAGradientLayer()
            gradientLayer.colors = [topColor, bottomColor]
            gradientLayer.locations = [0.0, 1.0]
            gradientLayer.frame = self.bounds
                    
        self.layer.addSublayer(gradientLayer)
    }
    
    
    func setRoundView() {
        //Setting aspect ratio of the labels in this app's case to 1:1 makes a square and setting the UI to a circle
        self.layer.cornerRadius = min(self.bounds.size.width/2, self.bounds.size.height/2)//self.frame.width/2
        self.layer.masksToBounds = true
    }
}

extension CGFloat {
    static var random: CGFloat {
        return CGFloat(arc4random()) / CGFloat(UInt32.max)
    }
}

extension UIColor {
    static var random: UIColor {
        return UIColor(red: .random, green: .random, blue: .random, alpha: 1.0)
    }
}

extension String {
    func extractString(from: String, to: String) -> String? {
        return (range(of: from)?.upperBound).flatMap { substringFrom in
            (range(of: to, range: substringFrom..<endIndex)?.lowerBound).map { substringTo in
                String(self[substringFrom..<substringTo])
            }
        }
    }
}
