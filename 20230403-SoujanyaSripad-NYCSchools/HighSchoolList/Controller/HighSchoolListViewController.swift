//
//  HighSchoolListViewController.swift
//  20230303-SoujanyaSripad-NYCSchools
//
//  Created by Soujanya on 05/03/23.
//

import UIKit
import MapKit

protocol HighSchoolListViewControllerDelegate: AnyObject {
    func schoolSelected()
}

//This class handles the display of list of schools with a search option.

//Given more time, I would prefer to setup the location based schools filtering and add to favourites option for easy access for future reference.

//Following a MVVM model where in the processing and fetching of UI element values is taken care in the view model

class HighSchoolListViewController: UIViewController {
   
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var noSearchResultsLabel: UILabel!
    fileprivate var viewModel: HighSchoolListViewModel!
    weak var delegate: HighSchoolListViewControllerDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        viewModel = HighSchoolListViewModel()
        viewModel.delegate = self
        viewModel.getHighSchoolList()
        self.activityIndicator.stopAnimating()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        // turn on the activity indicator
        if !viewModel.hasSchools {
            activityIndicator.startAnimating()
        }
        
        if self.searchBar.text == "" {
            self.noSearchResultsLabel.isHidden = true
        }
        
    }
    
    // MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        //Pass the selected school with details to the destination view controller
        if segue.identifier == "showSchoolDetail" {
            let highSchoolDetailVC = segue.destination as! HighSchoolDetailTableViewController
            if let selectedSchool = sender as? HighSchool {
                highSchoolDetailVC.selectedSchool = selectedSchool
                
            }
        }
    }
    
}

// MARK: - HighSchoolListViewModel Delegate
extension HighSchoolListViewController: HighSchoolViewModelDelegate {
    func error(with message: String) {
        AlertService.addAlert(in: self, with: message)
    }
    
    func reloadData() {
        DispatchQueue.main.async {[weak self] in
            self?.tableView.reloadData()
            self?.activityIndicator.stopAnimating()
        }
    }
}


extension HighSchoolListViewController : UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.totalRows()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    
        let cell = tableView.dequeueReusableCell(withIdentifier: "HighSchoolTableViewCell", for: indexPath) as! HighSchoolTableViewCell
        cell.configure(viewModel.cellViewModel(at: indexPath.row))
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let selectedSchool = viewModel.selectedSchool(at: indexPath.row)
        self.performSegue(withIdentifier: "showSchoolDetail", sender: selectedSchool)
    }
    
}

//Searching a text by filtering schools based on text entered by name
extension HighSchoolListViewController: UISearchBarDelegate
{
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
       let isSearching = viewModel.searchBar(textDidChange: searchText)
        if !isSearching {
            self.noSearchResultsLabel.isHidden = false
            self.view.bringSubviewToFront(noSearchResultsLabel)
        }
        else {
            self.noSearchResultsLabel.isHidden = true
        }
        self.tableView.reloadData()
    }
}

