//
//  HighSchoolTableViewCell.swift
//  20230303-SoujanyaSripad-NYCSchools
//
//  Created by Soujanya on 06/03/23.
//

import UIKit

class HighSchoolTableViewCell: UITableViewCell {
    
    @IBOutlet weak var schoolName: UILabel!
    @IBOutlet weak var schoolAddress: UILabel!
    @IBOutlet weak var schoolWebsite: UILabel!
    @IBOutlet weak var schoolPhoneNumber: UIButton!
    @IBOutlet weak var stackView: UIStackView!
    
    private var viewModel: HighSchoolTableCellViewModel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    @IBAction func phoneButtonClicked(_ sender: Any) {
        if let phone = self.schoolPhoneNumber.titleLabel?.text , let url = URL(string: "tel://" + phone) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        }
    }
    
    
    func configure(_ viewModel: HighSchoolTableCellViewModel) {
        self.viewModel = viewModel
        schoolName.text = viewModel.schoolName
        schoolAddress.text = viewModel.address
        schoolWebsite.text = viewModel.website
        schoolPhoneNumber.setTitle(viewModel.phoneNumber, for: .normal)
        schoolName.setContentCompressionResistancePriority(.required, for: .vertical) 
        stackView.dropInnerShadow(cornerRadius: 15.0)
        stackView.setGradientBackground()
        
    }
    
}
