//
//  HighSchoolListViewModel.swift
//  20230303-SoujanyaSripad-NYCSchools
//
//  Created by Soujanya on 06/03/23.
//

import Foundation

//This class takes care of providing data in the desired manner to the view, making network calls and avoids any unnecessary functions in controller making it loaded.

protocol HighSchoolViewModelDelegate : AnyObject {
    func error(with message: String)
    func reloadData()
}

class HighSchoolListViewModel {
    weak var delegate: HighSchoolViewModelDelegate?
    private var schools: [HighSchool] = []
    private var filteredSchools: [HighSchool]?
    var satScores: [SATResults] = []
    let satResultsViewModel = HighSchoolDetailViewModel()
    var hasSchools : Bool = false
    var isSearching : Bool = false
    
    func getHighSchoolList() {
        satResultsViewModel.delegate = self
        NetworkManager.getHighSchoolList(completion: {[weak self] (errorMessage, highSchools) in
            
            if errorMessage != nil {
                self?.delegate?.error(with: errorMessage!)
            }
            else {
                self?.schools = highSchools?.sorted {$0.school_name < $1.school_name } ?? []
                self?.hasSchools = true
                self?.satResultsViewModel.getSATResults()
                self?.delegate?.reloadData()
            }
        })
    }
}

//MARK: - Table methods
extension HighSchoolListViewModel {
    func totalRows() -> Int {
        return filteredSchools?.count ?? schools.count
    }
    
    func cellViewModel(at index: Int) -> HighSchoolTableCellViewModel {
        let school = filteredSchools?[index] ?? schools[index]
        return HighSchoolTableCellViewModel(school)
    }
    
    func selectedSchool(at index: Int) -> HighSchool {
        let school = filteredSchools?[index] ?? schools[index]
        return school
    }
    
}


extension HighSchoolListViewModel : HighSchoolDetailViewModelDelegate {
    func error(with message: String) {
        //
    }
    
    func reloadData() {
        //
    }
    
    func mapScoresToSchools(scores: [SATResults]) {
        //Mapping satResults to respective schools using higher order functions.
        scores.forEach { score in
            guard var school = schools.filter({ $0.dbn == score.dbn }).first, let index = schools.firstIndex(where: {$0.dbn == score.dbn }) else { return }
            schools.remove(at: index)
            school.SATResults = score
            schools.insert(school, at: index)
        }
    }
}


extension HighSchoolListViewModel {
    func searchBar(textDidChange searchText: String) -> Bool {
        
        // This method updates filteredData based on the text in the Search Box
        // When there is no text, filteredData is the same as the original data
        // When user has entered text into the search box
        // Use the filter method to iterate over all items in the data array
        // For each item, return true if the item should be included and false if the
        // item should NOT be included
        filteredSchools = searchText.isEmpty ? schools : schools.filter { (school) -> Bool in
            // If dataItem matches the searchText, return true to include it
            return school.school_name.range(of: searchText, options: .caseInsensitive, range: nil, locale: nil) != nil
        }
        
        if filteredSchools!.count > 0 {
            isSearching = true
            return true
        }
        isSearching = false
        return false
    }
    
}
