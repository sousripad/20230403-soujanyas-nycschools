//
//  HighSchoolTableCellViewModel.swift
//  20230303-SoujanyaSripad-NYCSchools
//
//  Created by Soujanya on 06/03/23.
//

import Foundation
import MapKit

//This class creates an easy access to the configurable items in the school table view cell adn provides the necessary data to be presented.
//Using MapKit to create a pin to display the school location on map

class HighSchoolTableCellViewModel {
    private var highSchool: HighSchool!
    
    
    init(_ school: HighSchool) {
        self.highSchool = school
    }
    
    var schoolName: String {
        return highSchool.school_name
    }
    
    var address: String {
        //Beautifying the address removing location coordinates
        let school_address = highSchool.location?.components(separatedBy: "(")
        return school_address?[0] ?? ""
    }
    
    var website: String {
        return highSchool.website ?? ""
    }
    
    var phoneNumber: String? {
        return highSchool.phone_number
    }
    
    var overview: String? {
        return highSchool.overview_paragraph
    }
    
    var satAvgReadingScore: String? {
        return highSchool.SATResults?.sat_critical_reading_avg_score
    }
    
    var satAvgMathScore: String? {
        return highSchool.SATResults?.sat_math_avg_score
    }
    
    var satAvgWritingScore: String? {
        return highSchool.SATResults?.sat_writing_avg_score
    }
    
    var directions: String? {
        return NSLocalizedString("Directions", comment: "Directions")
    }
    
    var coordinates: CLLocationCoordinate2D? {
        if let schoolAddress = highSchool.location {
            if let coordinateString = schoolAddress.extractString(from: "(", to: ")") {
                let coordinates = coordinateString.components(separatedBy: ",")
                let latitude = (coordinates[0] as NSString).doubleValue
                let longitutde = (coordinates[1] as NSString).doubleValue
                return CLLocationCoordinate2D(latitude: CLLocationDegrees(latitude) , longitude: CLLocationDegrees( longitutde))
            }
        }
        return nil
    }
    
    var hasSATScore: Bool? {
        if highSchool.SATResults != nil {
            return true
        }
        return false
    }
    
    
    //Make a call on click of phone number
    private func callNumber(phoneNumber:String) {
        
        if let phoneCallURL = URL(string: "telprompt://\(phoneNumber)") {
            
            let application:UIApplication = UIApplication.shared
            if (application.canOpenURL(phoneCallURL)) {
                if #available(iOS 10.0, *) {
                    application.open(phoneCallURL, options: [:], completionHandler: nil)
                } else {
                    // Fallback on earlier versions
                    application.openURL(phoneCallURL as URL)
                    
                }
            }
        }
    }
}
