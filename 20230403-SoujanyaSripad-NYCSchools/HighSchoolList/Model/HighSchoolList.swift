//
//  HighSchoolList.swift
//  20230303-SoujanyaSripad-NYCSchools
//
//  Created by Soujanya on 04/03/23.
//

import Foundation

//This is a model of the highSchool parameters we will consume from the API service. So when we recieve the API response, we will get a list/array of HighSchools with each having the following parameters mapped. We are using Encoding&Decoding by making this struct codable.

//Using a SATResults struct separately for easy mapping as the API call is distinct

struct HighSchool: Codable {
    let dbn: String
    let school_name: String
    let overview_paragraph: String?
    let location: String?
    let website:String?
    let phone_number: String?
    let language_classes: String?
    let school_email: String?
    var SATResults : SATResults?
}


struct HighSchoolList: Codable {
    let highSchoolList: [HighSchool]
    
    static func highSchools(from json: [String: Any]?) -> [HighSchool]? {
        do {
            let decoder = JSONDecoder()
            let data = try JSONSerialization.data(withJSONObject: json ?? [:], options: .prettyPrinted)
            let response = try decoder.decode(HighSchoolList.self, from: data)
            return response.highSchoolList
        } catch {
            return nil
        }
    }
}
